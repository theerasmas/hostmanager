//! A module for file reading, file writing, and printing to the terminal.
//! Used by `main.rs`.

use std::io::prelude::*;
use std::io::Error;
use std::fs::OpenOptions;
use console::style;
use std::fmt::Display;
use hosts_file::HostsFile;
use std::env;

/// Will read the /etc/hosts file (or another filepath specified in the
/// `HOSTS_FILE` environment variable.
/// Returns a `Result` with an instance of `HostsFile` or a `String` error.
pub fn read_hosts_file() -> Result<HostsFile, String> {

    let filepath = get_hosts_filepath();

    read_file(filepath)
        .map(HostsFile::from_hosts_file)
        .map_err(|e| format!("{}", e))

}

/// Attempts to overwrite the hosts file using the formatted `HostsFile`
/// contents.
///
/// # Errors
///
/// Will return `Err(underlying_io_error)` whenever the write fails.
///
pub fn overwrite_hosts_file(hosts_file: &HostsFile) -> Result<(), Error> {
    let filepath = get_hosts_filepath();

    let contents = hosts_file.format_all();
    let bytes = &contents.into_bytes();

    let mut file = OpenOptions::new()
        .write(true)
        .truncate(true)
        .open(filepath)?;

    file.write_all(bytes)
}

/// Will print the given (displayable) element `s` to the console in red.
/// Used by `main.rs` to alert the user to failures.
pub fn print_red<T: Display>(s: T) {
    println!("{}", style(s).red())
}

/// Attempts to read the given `filename` and will return
/// a `Result` with either the `String` contents or the underlying `Error`.
///
/// # Errors
///
/// Will return `Err(underlying_io_error)` whenever the file cannot be read.
///
fn read_file(filename: String) -> Result<String, Error> {

    let open_file = OpenOptions::new()
        .read(true)
        .open(filename);

    let mut contents = String::new();

    match open_file.and_then(|mut f| f.read_to_string(&mut contents)) {

        Ok(_)  => Ok(contents),
        Err(e) => Err(e)

    }
}

/// Obtain the path to the hosts file, either /etc/hosts or the value
/// specified in the HOSTS_FILE variable
fn get_hosts_filepath() -> String {
    env::var("HOSTS_FILE")
        .unwrap_or(String::from("/etc/hosts")
        )
}
