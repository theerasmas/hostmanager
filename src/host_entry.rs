//! This module contains the `HostEntry` type used for managed host entries in
//! the `HostsFile` struct.

use std::net::IpAddr;

use comment::Comment;
use hostnames::Hostnames;
use printer::Printable;


/// This struct represents an individual line in an /etc/hosts file. It is only
/// used for managed entries (entries read and written to by this program).
/// This lets us expand the notion of a host entry to include a unique
/// identifier (the `name` field) and whether or not it is `active`.
#[derive(PartialEq, Clone, Debug)]
pub struct HostEntry {

    hostnames: Hostnames,
    ip_address: IpAddr,
    comment: Comment,
    active: bool,
}



impl HostEntry {
    /// Public function to construct a new `HostEntry` instance.
    pub fn new(ip_address: IpAddr,
               hostnames: Hostnames,
               comment: Comment,
               active: bool) -> HostEntry {

        HostEntry { hostnames, ip_address, comment, active }
    }

    /// A caller can modify any of this entry's fields *except* for its label
    /// (for that, use `rename`). The options types are designed to work with
    /// the `ArgMatches` values used by `cli.hs`, since that is the only place
    /// this method is currently used.
    pub fn modify(&mut self,
                  ip_address: Option<IpAddr>,
                  hostnames: Option<Hostnames>,
                  comment: Option<Comment>,
                  active: Option<bool>) {

        if let Some(i) = ip_address { self.ip_address = i }

        if let Some(h) = hostnames { self.hostnames = h; }

        if let Some(c) = comment { self.comment = c; }

        if let Some(a) = active { self.active = a; }
    }


    /// Check if the `HostEntry` is currently active
    pub fn is_active(&self) -> bool {
        self.active
    }

    /// We often need to print out the status as active or inactive
    pub fn active_string(&self) -> String {
        if self.active { "active".to_string() } else { "inactive".to_string() }
    }

    /// Check if any of the given `Hostnames` are already present in this entry.
    pub fn has_any_hosts(&self, hosts: &Hostnames) -> bool {
        self.hostnames.contains_any(hosts)
    }

    /// The formatted entry that will be written to the hosts file.
    pub fn format(&self) -> String {
        let mut s = String::new();

        s.push_str(&format!("comment={}>\n", self.comment));

        let maybe_comment_out = if self.active { "" } else { "# " };

        s.push_str(&format!("{}{} ", maybe_comment_out, self.ip_address));

        s.push_str(&format!("{}\n\n", self.hostnames));

        s
    }
}

impl Printable for HostEntry {

    /// This method is used to format a managed `HostEntry` for printing to
    /// the console. The main application uses this for printing status updates.
    fn format_console(&self) -> String {
        let mut s = String::new();

        s.push_str(&format!("IP Address: {}\n", self.ip_address));
        s.push_str(&format!("Hosts: {}\n", &self.hostnames));

        if self.comment.is_not_empty() {
            s.push_str(&format!("Comment: {}\n", self.comment));
        }

        s.push_str("\n");

        s
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_create_host_entry() {
        let ip = IpAddr::V4("127.0.0.1".parse().unwrap());
        let hosts = "one two three\n".parse().unwrap();
        let comment: Comment = "this is a comment".parse().unwrap();
        let active = true;
        let entry = HostEntry::new(ip,
                                   hosts,
                                   comment.clone(),
                                   active);
        let expected_hosts: Hostnames = "one two three".parse().unwrap();

        assert_eq!(entry.ip_address, ip);
        assert_eq!(entry.hostnames, expected_hosts);
        assert_eq!(entry.comment, comment);
        assert_eq!(entry.active, true);
    }

    #[test]
    fn test_modify_host_entry() {
        let ip = IpAddr::V4("127.0.0.1".parse().unwrap());
        let hosts = "one two three\n".parse().unwrap();
        let comment: Comment = "this is a comment".parse().unwrap();
        let active = true;
        let mut entry = HostEntry::new(ip,
                                       hosts,
                                       comment.clone(),
                                       active);

        entry.modify(None,
                     Some("foo bar baz\n".parse().unwrap()),
                     None,
                     Some(false));

        let expected_hosts: Hostnames = "foo bar baz".parse().unwrap();

        assert_eq!(entry.ip_address, ip);
        assert_eq!(entry.hostnames, expected_hosts);
        assert_eq!(entry.comment, comment);
        assert_eq!(entry.active, false);
    }
}
