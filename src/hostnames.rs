//! This module contains the `Hostnames` type used for managed host entries. It
//! provides several convenience methods for working with hostnames as
//! space-delimited `String`s, making it easier to deal with external inputs
//! and check for duplicates.

use std::fmt;
use std::str::FromStr;
use std::error::Error;
use parser::{unmanaged_host_entry_parser, valid_host_string_parser};


#[derive(PartialEq, Clone, Debug)]
pub struct Hostnames {
    hostnames: Vec<String>,
}

impl Hostnames {

    /// Create a new `Hostnames` from `Vec<String>`. We could validate the
    /// `String`s here but since we only use this function after using
    /// `parser.rs` we know the strings should not contain spaces, control
    /// characters, or anything else problematic.
    pub fn new(hostnames: Vec<String>) -> Hostnames {
        Hostnames { hostnames }
    }

    /// Tests if a given host `str` is a member of this `Hostnames` instance.
    pub fn contains(&self, host: &str) -> bool {
        self.hostnames
            .iter()
            .find(|ref h| h == &&host)
            .is_some()
    }

    /// Tests if any hosts from `other` are in this `Hostnames` instance.
    /// Complexity is worst case O(m*n) but the most common case is a single
    /// hostname.  Even in rare cases where someone uses many aliases, many
    /// `/etc/hosts` implementations have hard limits on line size (~1000
    /// characters) or max aliases (anywhere from 9-34). If this is a bottleneck
    /// in practice, the vectors can be converted to `HashSet`s first to obtain
    /// the intersection.
    pub fn contains_any(&self, other: &Hostnames) -> bool {
        self.hostnames
            .iter()
            .find(|ref h| other.contains(h))
            .is_some()
    }

    /// Takes any unmanaged lines from the hosts file and returns either
    /// `Some(hostnames)` or `None`.
    pub fn from_raw_host_entry(line: &String) -> Option<Hostnames> {
        let bytes = line.clone().into_bytes();

        unmanaged_host_entry_parser(&bytes).to_result().ok()
    }

}

impl fmt::Display for Hostnames {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let formatted = self.hostnames
            .iter()
            .fold(String::new(), | acc, ref host | acc + " " + &host);

        write!(f, "{}", formatted.trim())
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct HostnamesParseError;

impl Error for HostnamesParseError {
    fn description(&self) -> &str {
        "invalid hostname: valid characters are A-z, 0-9, _, -, and ."
    }
}


impl fmt::Display for HostnamesParseError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Unable to parse the requested hostname(s). Valid characters are: A-z, 0-9, '-', '_', and '.'")
    }
}

impl FromStr for Hostnames {
    type Err = HostnamesParseError;

    // TODO: should probably split on spaces here and then check each hostname
    // individually to give a better error. if an extra parser in parser.rs
    // can check for remaining bytes it can decide how to return.
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match valid_host_string_parser(s.as_bytes()).to_result() {
            Ok(hostnames) => Ok(hostnames),
            Err(_)        => Err(HostnamesParseError),
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_hosts_format() {
        let hostnames: Hostnames = "one two three".parse().unwrap();
        let expected = String::from("one two three");
        let res = format!("{}", hostnames);
        assert_eq!(res, expected);
    }

    #[test]
    fn test_hosts_contains() {
        let hostnames: Hostnames = "one two three".parse().unwrap();
        assert!(hostnames.contains("one"));
        assert!(hostnames.contains("two"));
        assert!(hostnames.contains("three"));
        assert!(!hostnames.contains("other"));
    }

}
