//! Each public function here represents a subcommand in our program, and is
//! used by `main.rs`.
//!
//! Note: we rely on the `clap` library and `main.rs`'s usage of subcommands
//! to ensure that all required arguments are present in `ArgMatches`
//! by the time these functions are called. This allows us to call
//! `.unwrap()` on many `ArgMatches` values, because the program would have
//! failed at runtime already if they were not present.

use std::str::FromStr;
use std::fmt::Display;
use std::error::Error;
use std::net::IpAddr;
use clap::{ArgMatches};
use super::io::{read_hosts_file, overwrite_hosts_file, print_red};
use super::comment::Comment;
use super::label::Label;
use super::hostnames::Hostnames;

use console::style;


// We only use errors in formatted output, so `Box<Error>` is sufficient
type CommandResult<T> = Result<T, Box<Error>>;

/// will attempt to add a new managed entry to our hosts file and will print
/// a success or failure message.
pub fn add(args: &ArgMatches) -> CommandResult<String> {

    let label: Label = parse_required_arg(args.value_of("label"))?;

    let ip_address: IpAddr = parse_required_arg(args.value_of("ip"))?;

    let hostnames: Hostnames = parse_required_arg(args.value_of("hosts"))?;

    // since comments are optional we default to an empty string
    let comment: Comment = args.value_of("comment").unwrap_or("").parse()?;

    let mut hosts_file = read_hosts_file()?;

    hosts_file.add_new_entry(
        label.clone(),
        ip_address,
        hostnames,
        comment
    )?;

    overwrite_hosts_file(&hosts_file)?;

    let status = &hosts_file.status(Some(&label))?;

    Ok(prepend_success_msg("Entry added", status))

}

/// will attempt to modify the given entry with one or more optionally changed
/// fields. prints the status of the modified entry on success, otherwise a
/// failure message if either:
///   1) the named entry is not found, or
///   2) the requested hosts conflict with hosts already in the hosts file
pub fn modify(args: &ArgMatches) -> CommandResult<String> {

    let active = maybe_active_flag(args);

    let label: Label = parse_required_arg(args.value_of("label"))?;

    let ip_address = maybe_parse_arg(args.value_of("ip"))?;

    let comment = maybe_parse_arg(args.value_of("comment"))?;

    let hostnames = maybe_parse_arg(args.value_of("hosts"))?;

    let mut hosts_file = read_hosts_file()?;

    hosts_file.modify_entry(label.clone(),
                            ip_address,
                            hostnames,
                            comment,
                            active)?;

    overwrite_hosts_file(&hosts_file)?;

    let status = &hosts_file.status(Some(&label))?;

    let success_msg = format!("Entry '{}' modified.", label);

    Ok(prepend_success_msg(success_msg, status))

}

/// will attempt to remove the given entry and either return a success message
/// (and an updated status of managed host file entries) or a failure message
/// if the requested entry is not found.
pub fn remove(args: &ArgMatches) -> CommandResult<String> {
    let mut hosts_file = read_hosts_file()?;

    let label = parse_required_arg(args.value_of("label"))?;

    hosts_file.remove_entry(&label)?;

    overwrite_hosts_file(&hosts_file)?;

    let status = &hosts_file.status(None)?;

    let success_msg = format!("Entry '{}' removed", label);

    Ok(prepend_success_msg(success_msg, status))

}

/// will attempt to rename the entry and provide a success message, or, if
/// no entry by the given name was found, a failure message.
pub fn rename(args: &ArgMatches) -> CommandResult<String> {

    // the `clap` library verifies that these values are present if we've
    // gotten this far.
    let old_label: Label = parse_required_arg(args.value_of("label"))?;
    let new_label: Label = parse_required_arg(args.value_of("new label"))?;

    let mut hosts_file = read_hosts_file()?;

    hosts_file.rename_entry(old_label.clone(), new_label.clone())?;

    overwrite_hosts_file(&hosts_file)?;

    let status = &hosts_file.status(Some(&new_label))?;

    let success_msg = format!("Entry '{}' renamed to '{}'",
                              old_label,
                              new_label);

    let output = prepend_success_msg(success_msg, status);

    Ok(output)
}

/// gets the status of our managed host entries. will optionally provide the
/// status of one entry if the user called subcommand `status <some entry>`.
pub fn status(args: &ArgMatches) ->  CommandResult<String> {
    let mut hosts_file = read_hosts_file()?;

    let active = maybe_active_flag(args);

    hosts_file.set_filter(active);

    let output = match args.value_of("label") {
        Some(label_str) => {
            let label = label_str.parse()?;
            hosts_file.status(Some(&label))?
        },
        None            => hosts_file.status(None)?
    };

    Ok(output)
}


/// Captures the common pattern of taking a required arg match value and
/// parsing it into the type required by our program.
fn parse_required_arg<T: 'static + FromStr>(arg: Option<&str>) -> CommandResult<T>
    where T::Err: Error {
    Ok(arg.unwrap().parse()?)
}

/// When modifying a host entry we need to deal with three cases: 1) the user
/// did not supply a value, 2) the user supplied an invalid value, and 3) the
/// user supplied a valid value. This function's success value is an `Option`
/// type so we can leave it as `None` if the user did not supply a value.
fn maybe_parse_arg<T: 'static + FromStr>(arg: Option<&str>) -> CommandResult<Option<T>>
    where T::Err: Error {
    match arg {
        Some(val) => Ok(Some(val.parse()?)),
        None      => Ok(None),
    }
}

/// The `--active` and `--inactive` flags are optional, so we need to check
/// when a user wants one of them or none at all.
fn maybe_active_flag(args: &ArgMatches) -> Option<bool> {
    if args.is_present("active") || args.is_present("inactive") {
        Some(args.is_present("active"))
    }
    else { None }
}

fn prepend_success_msg<T: Display>(message: T, output: &str) -> String {
    format!("{}\n\n{}", style(message).green(), output)
}

pub fn run_command(args: &ArgMatches,
                   runner: &Fn(&ArgMatches) -> CommandResult<String>) {
    match runner(args) {
        Ok(res) => {
            println!("{}", res);
        },
        Err(e) => print_red(e)
    }
}
