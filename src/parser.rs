//! This module parses an `/etc/hosts` file using the `nom` crate. It makes use
//! of the `HostLines` enum in `hosts_file.rs` to easily parse both managed
//! and unmanaged host entries (this is done so that if someone manually
//! rearranges the entries in their file, we can still find them in any order).

use nom::{space, is_space, is_digit, is_alphabetic, eol, multispace};
use std::str;
use std::str::FromStr;
use std::net::IpAddr;

use comment::Comment;
use label::Label;
use host_entry::HostEntry;
use hosts_file::HostLines;
use hostnames::Hostnames;

// The top level parser for obtaining a vector of `HostLines`.
named!(pub host_file_parser<&[u8], Vec<HostLines> >,
       many1!(alt!(managed_entries | anything_else))
);

// Parses managed entries into `HostLines` instances
named!(managed_entries<&[u8], HostLines>,
       do_parse!(entry: managed_host_entry_parser
                 >> (HostLines::Managed(entry))
       )
);


// Used as an alternate to `managed_entries` to get any other lines from
// the hosts file.
named!(anything_else<&[u8], HostLines>,
      do_parse!(
          line: map_res!(terminated!(take_while!(call!(|c| c != '\n' as u8)), end_of_line),
                   str::from_utf8)
              >> (HostLines::Unmanaged(String::from(line) + "\n"))
      )
);

// Lets the `anything_else` parser take lines up to '\n' or EOF.
named!(end_of_line, alt!(eof!() | eol));


// When a user attempts to add or modify managed host entries, this parser
// is used to parse out the `Hostnames` from unmanaged entries to ensure there
// are no duplicates. Since we only use this on `HostsFile` unmanaged entries
// that always end in '\n', we can use a host string parser that requires `\n`.

// TODO: see if we need to match a newline or space here. it may be better
// not to because if we feed in an entry like "localhost # something" then
// the valid_host_string_parser will correctly pick up only "localhost",
// but we need tests to verify this behavior
named!(pub unmanaged_host_entry_parser<&[u8], Hostnames>,
       do_parse!(ip_parser
                 >> space
                 >> hosts: valid_host_string_parser
                 >> (hosts)
       )
);


// Attempts to parse a complete `HostEntry`.
named!(managed_host_entry_parser<&[u8], (Label, HostEntry)>,
       do_parse!(label: label_parser
                 >> comment: managed_comment_parser
                 >> inactive: opt!(tag!("# ")) // for inactive host entries
                 >> ip: ip_parser
                 >> space
                 >> hosts: valid_host_string_parser
                 >> char!('\n')
                 >> opt!(multispace)
                 >> (label, HostEntry::new(ip, hosts, comment, inactive.is_none()))
       )
);

// Parses a managed entry's label, which is also the start of a managed entry.
named!(label_parser<&[u8], Label>,
       do_parse!(tag!("### <hostmanager label=\"")
                 >> name: map_res!(take_until!("\" "),
                                    str::from_utf8
                 )
                 >> tag!("\" ")
                 >> (Label::new(name))
       )
);

// Parses a managed comment by matching the `### <comment>` prefix and
// consuming all characters until the next newline.
named!(managed_comment_parser<&[u8], Comment>,
       do_parse!(tag!("comment=")
                 >> opt!(space)
                 >> comment: map_res!(map_res!(take_until!(">\n"), str::from_utf8),
                                      Comment::from_str
                 )
                 >> tag!(">\n")
                 >> (comment)
       )
);

// Converts bytes to std::net::IpAddr using the `FromStr` trait.
named!(ip_parser<&[u8], IpAddr>,
       map_res!(
           map_res!(take_till!(is_space), str::from_utf8),
           (|s: &str| s.parse())
       )
);

// This is used directly by the `Hostnames` type, and also as part of the
// managed and unmanaged host entry parsers.
named!(pub valid_host_string_parser<&[u8], Hostnames>,
       do_parse!(hostnames: separated_list_complete!(space, valid_hostname)
                 >> (Hostnames::new(hostnames))
       )
);

// Note: internet hostnames must start with a letter or a digit and contain only
// characters (A-z), digits (0-9), or the `-` or `.` symbols. however, some
// hostnames for other uses can start with any of those symbols or even contain
// underscores. we use the more relaxed definition here so people can define
// whatever aliases suit them
named!(valid_hostname<&[u8], String>,
       do_parse!(
           host: map_res!(
               take_while!(call!(
                   |c| is_alphabetic(c) ||
                       is_digit(c)      ||
                       c == '.' as u8   ||
                       c == '-' as u8   ||
                       c == '_' as u8)
               ),
               str::from_utf8
           )
           >> (String::from(host))
       )
);

// For now labels have the same requirements as hostnames, since there's no
// compelling reason to accept any extra characters.
named!(pub valid_label<&[u8], Label>,
       do_parse!(
           label_string: valid_hostname
           >> (Label { name: label_string })
       )
);


#[cfg(test)]
mod test {
    use super::*;
    use nom::IResult;
    use nom::ErrorKind::MapRes;

    fn get_example_file_contents() -> String {
        let sample = r####"### <hostmanager label="example" comment=example>
127.0.0.1 example.com foo bar

192.168.1.10 localsite

### <hostmanager label="example2" comment=hi there>
# 192.168.1.11 notused

"####;
        println!("sample string: {:?}", sample);
        String::from(sample)
    }

    fn get_example_file_entries() -> Vec<HostLines> {
        let name = Label::new("example");
        let comment = "example".parse().unwrap();

        let managed_one = (name, HostEntry::new("127.0.0.1".parse().unwrap(),
                                                "example.com foo bar".parse().unwrap(),
                                                comment,
                                                true));

        let unmanaged_one = String::from("192.168.1.10 localsite\n");

        let unmanaged_two = String::from("\n");

        let managed_two = (Label::new("example2"),
                           HostEntry::new("192.168.1.11".parse().unwrap(),
                                          "notused".parse().unwrap(),
                                          "hi there".parse().unwrap(),
                                          false)
                           );

        vec![ HostLines::Managed(managed_one),
              HostLines::Unmanaged(unmanaged_one),
              HostLines::Unmanaged(unmanaged_two),
              HostLines::Managed(managed_two) ]
    }

    #[test]
    fn test_multiple_entries() {
        let sample = get_example_file_contents().into_bytes();
        let res = host_file_parser(&sample);
        let expected = get_example_file_entries();
        assert_eq!(res, IResult::Done(&b""[..],
                                      expected));
    }

    #[test]
    fn parse_ip_generic_v4_test() {
        let expected = IpAddr::V4("127.0.0.1".parse().unwrap());
        let res = ip_parser(&b"127.0.0.1"[..]);
        assert_eq!(res, IResult::Done(&b""[..], expected));
    }

    #[test]
    fn parse_ip_generic_v6_test() {
        let expected = IpAddr::V6("2001:0db8:85a3::8a2e:0370:7334"
                                  .parse()
                                  .unwrap()
        );
        let res = ip_parser(&b"2001:0db8:85a3::8a2e:0370:7334"[..]);
        assert_eq!(res, IResult::Done(&b""[..], expected));
    }

    #[test]
    fn parse_ip_fails() {
        let res = ip_parser(&b"foo"[..]);
        assert_eq!(res, IResult::Error(MapRes));
    }


    #[test]
    fn parse_host_entry_managed() {
        let ip = IpAddr::V4("127.0.0.1".parse().unwrap());
        let hosts: Hostnames = "example.com".parse().unwrap();
        let label = Label::new("example");
        let comment: Comment = "a comment".parse().unwrap();
        let expected_host_entry = (label, HostEntry::new(ip, hosts, comment.clone(), true));
        let hosts_file = &b"### <hostmanager label=\"example\" comment=a comment>\n127.0.0.1 example.com\n\n"[..];
        let res = managed_host_entry_parser(hosts_file);
        assert_eq!(res, IResult::Done(&b""[..], expected_host_entry));
    }

    #[test]
    fn test_parse_label() {
        let res = label_parser(&b"### <hostmanager label=\"example\" "[..]);
        let expected = Label::new("example");
        assert_eq!(res, IResult::Done(&b""[..], expected));
    }

    #[test]
    fn test_unmanaged_host_entry_no_newline() {
        let line = &b"127.0.0.1 foo bar baz"[..];
        let res = unmanaged_host_entry_parser(line);
        let hosts: Hostnames = "foo bar baz".parse().unwrap();
        assert_eq!(res, IResult::Done(&b""[..], hosts));
    }

    #[test]
    fn test_unmanaged_host_entry_inline_comment() {
        let line = &b"127.0.0.1 foo bar baz # some commentary"[..];
        let res = unmanaged_host_entry_parser(line);
        let hosts: Hostnames = "foo bar baz".parse().unwrap();
        assert_eq!(res, IResult::Done(&b" # some commentary"[..], hosts));
    }

    #[test]
    fn test_valid_hostnames() {
        let expected: Hostnames = "foo.bar baz".parse().unwrap();
        let res = valid_host_string_parser(&b"foo.bar  \t   baz"[..]);
        assert_eq!(res, IResult::Done(&b""[..], expected));
    }

    #[test]
    fn test_valid_hostname() {
        let expected = "foo".to_string();
        let res = valid_hostname(&b"foo"[..]);
        assert_eq!(res, IResult::Done(&b""[..], expected));
    }
}
