extern crate hostmanager;

use hostmanager::cli::{run_command, add, modify, status, remove, rename};
use hostmanager::app::get_app;


// the main entry point for our command line application
fn main() {
    // this defines the command line application and gets back an instance of
    // `ArgMatches` with any subcommands and flags the user supplied.
    let app_matches = get_app().get_matches();

    // next we match on the subcommands and pass the `ArgMatches` to the
    // corresponding function in our `cli` module.
    match app_matches.subcommand() {
        ("add",     Some(sub_m)) => { run_command(sub_m, &add)    },
        ("modify",  Some(sub_m)) => { run_command(sub_m, &modify) },
        ("status",  Some(sub_m)) => { run_command(sub_m, &status) },
        ("remove",  Some(sub_m)) => { run_command(sub_m, &remove) },
        ("rename",  Some(sub_m)) => { run_command(sub_m, &rename) },
        // if no subcommands are matched, we rely on `clap` to print the help
        // output to stdout. if it cannot print to stdout it will panic.
        _ => { get_app().print_help().unwrap() },
    }

}

