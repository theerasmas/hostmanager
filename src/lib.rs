//! The public and private modules for reading, writing, and otherwise managing
//! the `/etc/hosts` file.

#[macro_use]
extern crate nom;
extern crate clap;
extern crate console;

// public mods used by `main.rs`
pub mod app;
pub mod cli;

// all other internal mods
mod comment;
mod hostnames;
mod host_entry;
mod hosts_file;
mod io;
mod label;
mod parser;
mod printer;

